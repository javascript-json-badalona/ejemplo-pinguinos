const foto = document.querySelector('img');
const titulo = document.querySelector('title');
const parrafos = document.querySelectorAll('.parrafo');
const parrafoGrande = document.querySelector('.parrafo.grande');
const importante = parrafoGrande.querySelector('strong');
const contenedor = document.querySelector('.contenedor');
const boton = document.createElement('button');
boton.textContent = 'Enviar';
boton.classList.add('grande');
// boton.addEventListener('click', () => console.log('botón clicado'));

contenedor.addEventListener('click', e => {
  if (e.target.classList.contains('grande')) {
    console.log('Has clicado en el botón');
  }
});

document.querySelector('.texto-cta').addEventListener('click', e => {
  e.preventDefault();
  if (e.target.classList.contains('accion')) {
    console.log('Han ocurrido cosas chulísimas');
  } else {
    console.log('La vida es gris y aburrida');
  }
});

setTimeout(() => {
  contenedor.append(boton);
}, 2000);

/*setTimeout(() => {
  foto.src = 'https://meriodelacastaysuborregada.files.wordpress.com/2017/09/gatete1.jpg?w=634&h=312&crop=1';
  titulo.textContent = 'He hackeado tu web';
  for (let parrafo of parrafos) {
    parrafo.textContent = 'WEB HACKEADA';
  }
  contenedor.previousElementSibling.querySelector('h1').textContent = 'Web hackeada';
  contenedor.insertBefore(foto, parrafoGrande);
}, 2000);*/
